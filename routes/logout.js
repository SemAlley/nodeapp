var express = require('express');
var router = express.Router();
var auth = require('../auth');

router.all('/', function (req, res, next) {
    auth.logout(req);
    res.redirect('/');
});

module.exports = router;
