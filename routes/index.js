var express = require('express');
var router = express.Router();
var auth = require('../auth');
router.all('/', function (req, res, next) {
    auth.check(req, res, function(){
        res.render('index', {title: 'Express'});
    });
});

module.exports = router;
