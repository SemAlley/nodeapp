var express = require('express');
var router = express.Router();
var auth = require('../auth');
var users = require('../models/user');

router.get('/', function (req, res, next) {
    auth.check(req, res, function () {
        res.render('users', {
            title: 'Users',
            data: users.findAll()
        });
    });
});

module.exports = router;
