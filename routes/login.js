var express = require('express');
var router = express.Router();
var auth = require('../auth');

router.all('/', function (req, res, next) {
    if (auth.checkBool(req, res)) {
        res.redirect(req.headers['referer'] ? req.headers['referer'] : '/');
        return;
    }

    if (auth.login(req)) {
        res.redirect('/');
    } else {
        var loginFormClass = req.body.username && req.body.password ? 'has-error' : '';
        res.render('login',
            {
                title: 'Login',
                loginFormClass: loginFormClass,
                username: req.body.username
            });
    }
});

module.exports = router;
