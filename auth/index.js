var user = require('../models/user');

module.exports = {

    users: user.findAll(),

    check: function (req, res, callback) {
        if (this.validateToken(req.auth.authToken)) {
            req.app.locals.isGuest = false;
            callback();
        } else {
            req.app.locals.isGuest = true;
            res.redirect('/login');
        }
    },
    checkBool: function (req, res) {
        if (this.validateToken(req.auth.authToken)) {
            req.app.locals.isGuest = false;
            return true;
        } else {
            req.app.locals.isGuest = true;
            return false;
        }
    },
    validateToken: function (authToken) {
        if (!authToken) {
            return false;
        }
        var username = authToken.match(/(.*)&&MegaShifrator&&/)[1];
        return this.users[username] !== undefined;
    },
    login: function (req) {
        if (this.users[req.body.username] && this.users[req.body.username]['password'] === req.body.password) {
            req.auth.authToken = req.body.username + '&&MegaShifrator&&';
            return true;
        }
        return false;
    },
    logout: function (req) {
        req.app.locals.isGuest = true;
        req.auth.reset();
    }

};